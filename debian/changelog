hdf-compass (0.7~b15-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.7~b15
  * Fix watchfile
  * Refresh patch
  * Do not use Nose for autopkgtest (Closes: #1018372)
  * Switch to DebHelper 13

 -- Alexandre Detiste <tchet@debian.org>  Fri, 09 Aug 2024 23:16:14 +0200

hdf-compass (0.7~b8-3) unstable; urgency=medium

  * Team upload.
  * debian patch matplotlib_backend_test.patch sets matplotlib backend
    to 'Agg' instead of 'WxAgg' when running in headless mode (when
    DISPLAY is not defined)
  * Standards-Version: 4.5.0

 -- Drew Parsons <dparsons@debian.org>  Sun, 22 Mar 2020 10:53:06 +0800

hdf-compass (0.7~b8-2) unstable; urgency=medium

  * Team upload.
  * mark hdf-compass-doc as Multi-Arch: foreign
  * add debian/tests (autopkgtest)
    - exclude components which need additional modules that are not
      available: adios bag hdf5rest opendap
    - Restrictions: allow_stderr for can_handle() consistency check
      and wx.lib.pubsub DeprecationWarning

 -- Drew Parsons <dparsons@debian.org>  Sun, 15 Sep 2019 13:19:27 +0800

hdf-compass (0.7~b8-1) unstable; urgency=medium

  * Team upload.
  * New upstream [pre]release.
  * replace python-hdf-compass with python3-hdf-compass
    (drop Python2). Closes: #936694.
    - Build-Depends: python3-wxgtk4.0, update py3dist-overrides
  * Standards-Version: 4.4.0
  * debhelper compatibility level 12: debhelper-compat (= 12)
    - update doc-base path to /usr/share/doc/hdf-compass
  * update Vcs tags to salsa.debian.org

 -- Drew Parsons <dparsons@debian.org>  Sat, 14 Sep 2019 05:28:48 +0800

hdf-compass (0.6.0-1) unstable; urgency=low

  * Initial release. (Closes: #812434)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 02 Jun 2016 11:57:42 +0100
