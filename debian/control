Source: hdf-compass
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: science
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               icoutils,
               python3-all,
               python3-h5py,
               python3-setuptools,
               python3-sphinx,
               python3-wheel,
               python3-wxgtk4.0
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/hdf-compass
Vcs-Git: https://salsa.debian.org/science-team/hdf-compass.git
Homepage: https://www.hdfgroup.org/projects/compass/

Package: hdf-compass
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-hdf-compass (= ${binary:Version})
Suggests: hdf-compass-doc
Description: viewer for HDF5 and related formats
 HDF Compass is an experimental viewer program for HDF5 and related formats,
 designed to complement other more complex applications like HDFView. Strong
 emphasis is placed on clean minimal design, and maximum extensibility through
 a plugin system for new formats.
 .
 This package provides the HDF Compass application.

Package: hdf-compass-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: documentation and examples for the HDF Compass
 HDF Compass is an experimental viewer program for HDF5 and related formats,
 designed to complement other more complex applications like HDFView. Strong
 emphasis is placed on clean minimal design, and maximum extensibility through
 a plugin system for new formats.
 .
 This package provides the documentation and examples for the HDF Compass
 application.

Package: python3-hdf-compass
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${python3:Depends}
Description: public modules for the HDF Compass
 HDF Compass is an experimental viewer program for HDF5 and related formats,
 designed to complement other more complex applications like HDFView. Strong
 emphasis is placed on clean minimal design, and maximum extensibility through
 a plugin system for new formats.
 .
 This package provides the public modules used by the HDF Compass application
 and third-party plugins.
